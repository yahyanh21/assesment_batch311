package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.JenisPakaian;
import com.example.demo.repository.JenisPakaianRepository;

@RestController
@RequestMapping("/master/")
public class JenisPakaianAPIController {
	
	@Autowired
	private JenisPakaianRepository jenisPakaianRepository;
	
	@GetMapping("jenispakaian")
	public ResponseEntity<List<JenisPakaian>> getAllJenis(){
		List<JenisPakaian> jenisPakaianList = this.jenisPakaianRepository.findAll();
		return new ResponseEntity<>(jenisPakaianList, HttpStatus.OK);
	}

	@PostMapping("jenispakaian/add")
	public ResponseEntity<Object> addJenisPakaian(@RequestBody JenisPakaian jenisPakaian){
		JenisPakaian pakaianData = this.jenisPakaianRepository.save(jenisPakaian);
		if(pakaianData.equals(jenisPakaian)) {
			return new ResponseEntity<>("add success", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("jenispakaian/edit/{id}")
	public ResponseEntity<Object> editJenisPakaian(@PathVariable("id") Long id,
			@RequestBody JenisPakaian jenisPakaian){
		Optional<JenisPakaian> jenisPakaianData = this.jenisPakaianRepository.findById(id);
		if(jenisPakaianData.isPresent()) {
			jenisPakaian.setId(id);
			this.jenisPakaianRepository.save(jenisPakaian);
			return new ResponseEntity<Object>("data updated", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
