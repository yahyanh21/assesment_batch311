package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.repository.JenisPakaianRepository;

@Controller
@RequestMapping("/master/")
public class JenisPakaianController {
	
	@Autowired
	private JenisPakaianRepository jenisPakaianRepository;

	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("jenisPakaian/index.html");
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView deletePakaian(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("redirect:/master/index");
		this.jenisPakaianRepository.deleteById(id);
		return view;
	}
	
	
	
}
