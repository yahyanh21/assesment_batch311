package com.example.demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.model.Orderan;
import com.example.demo.repository.OrderanRepository;

@RestController
@RequestMapping("/transaction/")
public class OrderanApiController {

	@Autowired
	private OrderanRepository orderanRepository;
	
	
	@GetMapping("orderan")
	public ResponseEntity<List<Orderan>> getAllJenis(){
		List<Orderan> listOrderan = this.orderanRepository.findAll();
		return new ResponseEntity<>(listOrderan, HttpStatus.OK);
	}
	
	@PostMapping("orderan/add")
	public ResponseEntity<Object> addOrderan(@RequestBody Orderan orderan){
		Orderan orderanData = this.orderanRepository.save(orderan);
		if(orderanData.equals(orderan)) {
			return new ResponseEntity<>("add success", HttpStatus.OK);
		}else {
			return new ResponseEntity<>("failed", HttpStatus.BAD_REQUEST);
		}
	}
	
	@PutMapping("orderan/edit/{id}")
	public ResponseEntity<Object> editOrderan(@PathVariable("id") Long id,
			@RequestBody Orderan orderan){
		Optional<Orderan> orderanData = this.orderanRepository.findById(id);
		if(orderanData.isPresent()) {
			orderan.setId(id);
			orderan.setOrderId(orderanData.get().getOrderId());
			this.orderanRepository.save(orderan);
			return new ResponseEntity<Object>("data updated", HttpStatus.OK);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
