package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.repository.OrderanRepository;

@Controller
@RequestMapping("/transaction/")
public class OrderanController {

	@Autowired
	private OrderanRepository orderanRepository;
	
	@GetMapping("index")
	public ModelAndView index() {
		ModelAndView view = new ModelAndView("order/index.html");
		return view;
	}
	
	@GetMapping("delete/{id}")
	public ModelAndView deletePakaian(@PathVariable("id") Long id) {
		ModelAndView view = new ModelAndView("redirect:/transaction/index");
		this.orderanRepository.deleteById(id);
		return view;
	}
	
}
