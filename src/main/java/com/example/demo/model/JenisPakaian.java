package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="jenis_pakaian")
public class JenisPakaian {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="id")
	private Long id;
	
	@Column(name="kategori_pakaian")
	private String kategoriPakaian;
	
	@Column(name="hargapersatuan")
	private Long hargaPerSatuan;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getKategoriPakaian() {
		return kategoriPakaian;
	}

	public void setKategoriPakaian(String kategoriPakaian) {
		this.kategoriPakaian = kategoriPakaian;
	}

	public Long getHargaPerSatuan() {
		return hargaPerSatuan;
	}

	public void setHargaPerSatuan(Long hargaPerSatuan) {
		this.hargaPerSatuan = hargaPerSatuan;
	}

	
	
	
}
