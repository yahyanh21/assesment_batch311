package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.JenisPakaian;

public interface JenisPakaianRepository extends JpaRepository<JenisPakaian, Long>{

}
