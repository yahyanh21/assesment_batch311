package com.example.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Orderan;

public interface OrderanRepository extends JpaRepository<Orderan, Long>{

}
